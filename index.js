let students = [];
var provider = new firebase.auth.GoogleAuthProvider();

firebase.auth().onAuthStateChanged(function (user) {
    toggleLogged(user);
});

const studentDb = firebase.firestore().collection('student');
studentDb.onSnapshot(data => {
    students = [];
    data.forEach(doc => students.push(Object.assign({id:doc.id}, doc.data())));
    display();
})

let classStart = moment().hours(9).minutes(30);

$('#btn-login').on('click', () => {
    firebase.auth().signInWithPopup(provider).then(() => toggleLogged(true));
});

$('#form-add').on('submit', event => {
    event.preventDefault();
    let name = $('#name').val();

    studentDb.add({ name: name, delays: [] });
});

$('#btn-modal').on('click', () => {
    display('#students-delays>.row', true);
    $('#students-delays .range').hide();
    $('#students-delays .today').show();
});

$('.btn-range').on('click', function () {
    $('#students-delays .range').hide();
    $('#students-delays .' + $(this).data('range')).show();
})

function display(targetSelector = '#students>.row', delays = false) {
    let target = $(targetSelector);
    target.empty();
    for (let student of students) {
        let div;
        if (!delays) {
            div = drawStudent(student);
        } else {
            div = drawDelay(student);
        }
        target.append(div);
    }
}

function drawStudent(student) {
    let div = $(`<div class="col-4 col-md-3"></div>`);
    let button = $(`<button class="btn btn-info">${student.name}</button>`);
    button.on('click', () => addDelay(student));
    let del = $('<button class="btn btn-danger ml-1">X</button>');
    del.on('click', () => {
        studentDb.doc(student.id).delete();
    });
    div.append(button);
    div.append(del);
    return div;
}

function drawDelay(student) {
    let div = $(`<div class="col-4"></div>`);
    let h3 = $(`<h3>${student.name}</h3>`);
    let ol = $(`<ol></ol>`);
    let totalDay = moment.duration();
    let totalWeek = moment.duration();
    let totalMonth = moment.duration();
    let totalAll = moment.duration();
    for (let delay of student.delays) {
        let time = moment.duration(delay.time, 'HH:mm');
        totalAll.add(time);

        let li = $(`<li class="range">${delay.date} &rArr; ${delay.time}</li>`);
        let date = moment(delay.date, 'DD/M/YYYY');

        if (classStart.dayOfYear() === date.dayOfYear()) {
            li.addClass('today');
            totalDay.add(time);
        }
        if (classStart.week() === date.week()) {
            li.addClass('week');
            totalWeek.add(time);
        }
        if (classStart.month() === date.month()) {
            li.addClass('month');
            totalMonth.add(time);
        }
        ol.append(li);
    }

    div.append(`<p>Total : 
        <span class="range today">${totalDay.get('hours')}:${totalDay.get('minutes')}</span>
        <span class="range week">${totalWeek.get('hours')}:${totalWeek.get('minutes')}</span>
        <span class="range month">${totalMonth.get('hours')}:${totalMonth.get('minutes')}</span>
        <span class="range all">${totalAll.get('hours')}:${totalAll.get('minutes')}</span>
    </p>`);
    div.append(h3);
    div.append(ol);
    return div;
}

function addDelay(student) {
    let delay = moment.utc(moment().diff(moment(classStart, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm");
    student.delays.push({ date: classStart.format('DD/M/YYYY'), time: delay });
    try {
        studentDb.doc(student.id).update({
            delays: student.delays
        });
    } catch(error) {
        alert('Action non autorisée. Veuillez vous connecter.')
    }
    

}

function toggleLogged(logged) {
    if (!logged) {
        $('.not-logged').show();
        $('.logged').hide();
    } else {
        $('.not-logged').hide()
        $('.logged').show();
    }
}